# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 14:28:11 2015

@author: janssen
"""

from zabbix_api import ZabbixAPI

zapi = ZabbixAPI(server="http://192.168.2.113/zabbix", log_level=6)
zapi.login("Admin", "zabbix")

zapi.host.massadd({
    "hosts": [
        {
            "hostid": 10158
        },
        {
            "hostid": 10159
        }
    ],
    "templates": [
        {
            "templateid": 10166
        }
    ]
})
