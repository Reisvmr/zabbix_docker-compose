# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 14:34:06 2015

@author: janssen
"""
#Import
from zabbix_api import ZabbixAPI
#Conecta
zapi = ZabbixAPI(server="http://127.0.0.1")
zapi.login("Admin", "zabbix")
hosts = zapi.host.get({
    "output":[
        "hostid",
        "host"],
        "sortfield": "name"
       # ,"sortorder": "DESC" #Ordem decrecente
        })

# Mostra Hosts
#print (hosts)
#for x in hosts:
    #print x['hostid'],"-", x['host']
