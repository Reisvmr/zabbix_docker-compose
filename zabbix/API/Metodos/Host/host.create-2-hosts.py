# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 14:28:11 2015

@author: janssen
"""

from zabbix_api import ZabbixAPI

zapi = ZabbixAPI(server="http://192.168.2.105/zabbix")
zapi.login("Admin","zabbix")

# armazenar a requisicao em uma variavel
# ter cuidado quando for copiar algum codigo
# prestar atenção na formatacao JSON
hostcriados = zapi.host.create([
    {
        "host": "Curso API para remover de novo 3",
        "status": 1,
        "interfaces": [
            {
                "type": 1,
                "main": 1,
                "useip": 1,
                "ip": "192.168.0.5",
                "dns": "",
                "port": 10050
            }
        ],
        "groups": [
            {
                "groupid": 2
            },
            {
                "groupid": 7
            }
        ],
        "templates": [
            {
                "templateid": 10001
            },
            {
                "templateid": 10047
            }
        ]
    },
    {
        "host": "Curso API para remover de novo 4",
        "status": 1,
        "interfaces": [
            {
                "type": 1,
                "main": 1,
                "useip": 1,
                "ip": "192.168.0.5",
                "dns": "",
                "port": 10050
            }
        ],
        "groups": [
            {
                "groupid": 2
            },
            {
                "groupid": 7
            }
        ],
        "templates": [
            {
                "templateid": 10001
            },
            {
                "templateid": 10047
            }
        ]
    }
])

#print "ID do host criado: ", hostcriado["hostids"][0]