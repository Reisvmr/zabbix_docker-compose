# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 14:28:11 2015

@author: janssen
"""

from zabbix_api import ZabbixAPI

zapi = ZabbixAPI(server="http://192.168.2.105/zabbix")
zapi.login("Admin","zabbix")

# armazenar a requisicao em uma variavel
hostcriado = zapi.host.create({
    "host": "Curso API 2",
    "status": 0, # habilitar monitoramento
    "interfaces": [
        {
            "type": 1, # agent zabbix
            "main": 1, # interface principal
            "useip": 1, # user conexao por IP
            "ip": "192.168.0.5", # o endereço IP
            "dns": "", # o nome DNS
            "port": 10050 # a porta de comunicação
        }
    ],
    "groups": [
        {
            "groupid": 2 # Linux servers
        }
    ],
    "templates": [
        {
            "templateid": 10001 # Template Linux
        }
    ]
})

print "ID do host criado: ", hostcriado["hostids"][0]